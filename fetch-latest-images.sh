#!/usr/bin/bash

imgs='torkeep argus'

repo_lookup_url='https://images.opencadc.org/api/v2.0/projects/caom2/repositories/'
repo_download_url='images.opencadc.org/caom2'

latest_versions=''
for img in ${imgs};do 
    version=`curl -s ${repo_lookup_url}/${img}/artifacts | jq '[.[].tags | select (. != null) | .[].name] | sort' | \
	grep -v "-" | grep -v ] | tail -1 | sed 's/"//g' | sed 's/,//g' | sed 's/ //g'`
    echo "Downloaded ${img}:${version}"
    latest_versions=${latest_versions}"${img}:${version}\n"
    sudo docker pull ${repo_download_url}/${img}:${version}
done

echo "Latest versions:"
echo -e "${latest_versions}"
