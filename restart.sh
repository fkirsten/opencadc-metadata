#!/bin/bash

# assumes that image versions in vars.yaml have been pulled via fetch-latest-images.sh and that vars.yaml has been updated
# will remove existing running containers and spin them up again

# "0" means it will not be touched, "1" means it will be restarted; CAREFUL with POSTGRES!
POSTGRES=0 # CAREFUL with POSTGRES! if removing do at docker commit -m "message" pg12db repo/name:tag first; then replace default image with repo/name:tag
ARGUS=1
TORKEEP=1
HAPROXY=1  # will be restarted in any case, but if set to 1 running container will be removed

set -e

# create a tempory set of variables from the (hopefully) existing vars.yaml
cat vars.yaml | sed 's/---/#/g' | sed 's/: /=/g' | sed 's/{{/${/g' | sed 's/}}/}/g' > /tmp/vars.sh
source /tmp/vars.sh

if ! [[ ${POSTGRES} -eq 0 ]]; then
    echo "Removing and restarting postgres db."
    sudo docker rm -f pg12db
    # Instantiate postgresql
    sudo docker run -d \
	   --volume=${CONFIG_FOLDER}/postgresql:/config:ro \
	   --volume=${LOGS_FOLDER}:/logs:rw \
	   -p 5432:5432 \
	   --name pg12db \
	   --network si-network \
	   ${POSTGRESQL_IMAGE}
    # Wait to populate
    sleep ${WAIT_SECONDS_LONG}
fi

if ! [[ ${ARGUS} -eq 0 ]]; then
    echo "Removing and restartin argus."
    sudo docker rm -f argus
    # Launch argus
    sudo docker run -d \
	   --volume=${CONFIG_FOLDER}/argus:/config:ro \
	   --volume=${CONFIG_FOLDER}/cadc-registry.properties:/config/cadc-registry.properties:ro \
	   --user tomcat:tomcat \
	   --name argus \
	   --network si-network \
	   ${REPO_PATH}/argus:${ARGUS_VERSION}
    # Wait to start up
    sleep ${WAIT_SECONDS}
fi

if ! [[ ${TORKEEP} -eq 0 ]]; then
    echo "Removing and restarting torkeep."
    sudo docker rm -f torkeep
    # Launch torkeep
    sudo docker run -d \
	   --volume=${CONFIG_FOLDER}/torkeep:/config:ro \
	   --volume=${CONFIG_FOLDER}/cadc-registry.properties:/config/cadc-registry.properties:ro \
	   --user tomcat:tomcat \
	   --name torkeep \
	   --network si-network \
	   ${REPO_PATH}/torkeep:${TORKEEP_VERSION}
    # Wait to start up
    sleep ${WAIT_SECONDS}
fi

if ! [[ ${HAPROXY} -eq 0 ]]; then
    echo "Removing and restarting haproxy."
    sudo docker rm -f haproxy
    # Instantiate haproxy
    sudo docker run -d \
	   --volume=${CERTIFICATES_FOLDER}/:/config:ro \
	   --volume=${LOGS_FOLDER}:/logs:rw \
	   --volume=${CONFIG_FOLDER}/haproxy:/usr/local/etc/haproxy/:rw \
	   -p ${HAPROXY_EXPOSED_PORT}:443 \
	   --name haproxy \
	   --network si-network \
	   ${HAPROXY_IMAGE}
    # Wait to populate
    sleep ${WAIT_SECONDS_LONG}
else
    echo "Restarting haproxy."
    sudo docker restart haproxy
fi
