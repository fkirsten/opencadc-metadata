#!/bin/bash

set -e

# create a tempory set of variables from the (hopefully) existing vars.yaml
cat vars.yaml | sed 's/---/#/g' | sed 's/: /=/g' | sed 's/{{/${/g' | sed 's/}}/}/g' > /tmp/vars.sh
source /tmp/vars.sh

# create required directories
mkdir -p ${LOGS_FOLDER}
mkdir -p ${CACERTIFICATES_FOLDER}

# Pull all the containers precreated
docker pull ${POSTGRESQL_IMAGE}
docker pull ${HAPROXY_IMAGE}
docker pull ${REPO_PATH}/argus:${ARGUS_VERSION}
docker pull ${REPO_PATH}/torkeep:${TORKEEP_VERSION}

# create the network to be used by all
docker network create si-network

# Instantiate postgresql
docker run -d \
       --volume=${CONFIG_FOLDER}/postgresql:/config:ro \
       --volume=${LOGS_FOLDER}:/logs:rw \
       -p 5432:5432 \
       --name pg12db \
       --network si-network \
       ${POSTGRESQL_IMAGE}
# Wait to populate
sleep ${WAIT_SECONDS_LONG}

# Launch argus
docker run -d \
       --volume=${CONFIG_FOLDER}/argus:/config:ro \
       --volume=${CONFIG_FOLDER}/cadc-registry.properties:/config/cadc-registry.properties:ro \
       --user tomcat:tomcat \
       --name argus \
       --network si-network \
       ${REPO_PATH}/argus:${ARGUS_VERSION}
# Wait to start up
sleep ${WAIT_SECONDS}

# Launch torkeep
docker run -d \
       --volume=${CONFIG_FOLDER}/torkeep:/config:ro \
       --volume=${CONFIG_FOLDER}/cadc-registry.properties:/config/cadc-registry.properties:ro \
       --user tomcat:tomcat \
       --name torkeep \
       --network si-network \
       ${REPO_PATH}/torkeep:${TORKEEP_VERSION}
# Wait to start up
sleep ${WAIT_SECONDS}

# Instantiate haproxy
docker run -d \
       --volume=${CERTIFICATES_FOLDER}/:/config:ro \
       --volume=${LOGS_FOLDER}:/logs:rw \
       --volume=${CONFIG_FOLDER}/haproxy:/usr/local/etc/haproxy/:rw \
       -p ${HAPROXY_EXPOSED_PORT}:443 \
       --name haproxy \
       --network si-network \
       ${HAPROXY_IMAGE}
# Wait to populate
sleep ${WAIT_SECONDS_LONG}


